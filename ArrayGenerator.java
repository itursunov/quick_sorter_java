package quick_sort;

public class ArrayGenerator {
	public static int[] generateArray(int size_) {
		int result[];
		result = new int[size_];
		for (int i = 0; i < size_; i++) {
			result[i] = (int)(Math.random() * 100);
		}
		return result;
	}
	
	public static int[] main(String[] args) {
		return generateArray(Integer.parseInt(args[0]));
	}
}
