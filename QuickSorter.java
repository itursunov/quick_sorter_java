package quick_sort;

import java.util.Arrays;


public class QuickSorter {
	
	private static int[] concatArrays(int[] a, int[] b) {
		int size_ = a.length + b.length;
		int[] result = new int[size_];
		System.arraycopy(a, 0, result, 0, a.length);
		System.arraycopy(b, 0, result, a.length, b.length);
		return result;
	}
	
	private static int[] sortArray(int[] unsortedArray) {
		if (unsortedArray.length <= 1) {
			return unsortedArray;
		}
		int head[]; int tail[];
		int headSize = 0; int tailSize = 0;
		int supportIndex = unsortedArray.length / 2;
		int supportElement = unsortedArray[supportIndex];
		for (int i = 0; i < unsortedArray.length; i++) {
			if (i == supportIndex) continue;
			if (unsortedArray[i] < supportElement) {
				headSize += 1;
			} else {
				tailSize += 1;
			}
		}
		head = new int[headSize]; tail = new int[tailSize];
		int currentHeadIndex = 0;int currentTailIndex = 0;
		for (int i = 0; i < unsortedArray.length; i++) {
			if (i == supportIndex) continue;
			if (unsortedArray[i] < supportElement) {
				head[currentHeadIndex] = unsortedArray[i];
				currentHeadIndex += 1;
			} else {
				tail[currentTailIndex] = unsortedArray[i];
				currentTailIndex += 1;
			}
		}
		int[] sortedHead = sortArray(head);
		int[] sortedTail = sortArray(tail);
		int[] supportArray = {supportElement};
		int[] result = concatArrays(
			concatArrays(sortedHead, supportArray),
			sortedTail
		);
		return result;
	}
	
	public static void main(String[] args) {
		int[] unsortedArray = ArrayGenerator.generateArray(300);
		int[] sortedArray = sortArray(unsortedArray);
		System.out.println(Arrays.toString(unsortedArray));
		System.out.println(Arrays.toString(sortedArray));
	}
}
